from flask import Flask, render_template, request, make_response
import sqlite3
import json
from model import quiz

def get_db_connection():
    conn = sqlite3.connect('static/database/database.db')
    conn.row_factory = sqlite3.Row
    return conn

def get_data_flower():
    conn = get_db_connection()
    flower = conn.execute('SELECT * FROM flower').fetchall()
    print(flower)
    return flower
    conn.close()

# import os

# PWD = os.getcwd()
# DIR_IMG = PWD+"/images/"
# if not os.path.exists(DIR_IMG):
#         os.makedirs(DIR_IMG)
# >>> flower = conn.execute('SELECT * FROM flower').fetchall()
#
# >>> flower # print data
# [<sqlite3.Row object at 0x7fd7b5888460>, <sqlite3.Row object at 0x7fd7b5616f50>, <sqlite3.Row object at 0x7fd7b547ba60>, <sqlite3.Row object at 0x7fd7b58d3910>, <sqlite3.Row object at 0x7fd7b54c21a0>]
#
# >>> flow = list()
# []
# >>> for k in flower : flow.append(k[:])
# ... 
# >>> flow
# [(1, 'Camellia', 'Camellia sasanqua', None, None, None), (2, 'Petite Pervenche', 'Vinca minor', None, None, None), (3, 'Pensée des champs', 'Viola arvensis', None, None, None), (4, 'Scabiosa', 'Scabiosa atropurpurea', None, None, None), (5, 'Populage des marais', 'Caltha palustris', None, None, None)]
# >>> flower[0].keys()
# ['id', 'name', 'name_sci', 'image_url', 'image_author', 'wiki_url']
#
# >=================================<
#       Generated item poper          
# >=================================<
# import secrets
# names = ["John", "Juan", "Jane", "Jack", "Jill", "Jean"]
# def selectRandom(names):
#     return secrets.choice(names)

import secrets
import requests
TOCKEN_API_ACCESS="KtV9tOqC2jI4xkr3CY94VuOwuGnUjrIPzcOkMvMJx8g"
quizz = quiz.Quiz()

def api_get_data_flower():
    host_api    = "https://trefle.io/api/v1"
    type_api    = "/species"
    token       = "?token="+TOCKEN_API_ACCESS
    filter_api  = "&filter[flower_conspicuous]=true" + "&filter[rank]=species"
    query = host_api+type_api+token+filter_api
    r = requests.get(query)
    if r.status_code == 200:
        return r.json()
    else:
        raise Exception(f"HTTP requests Errors - {r.status_code} -")

def choice_4flower(json_req):
    number_of_object = json_req["meta"]["total"]
    data = json_req["data"]
    choising = []
    # choosing 
    for k in range(4):
        choising.append(secrets.choice(data)) # json object
    # get one of 4 object taken
    the_question = secrets.choice(choising)
    the_question = {"id":the_question["id"],
        "name":the_question["common_name"],
        "sci_name":the_question["scientific_name"],
        "img_url":the_question["image_url"]}
    # get only commun_name
    flower = [{"id":k["id"],"name":k["common_name"],"sci_name":k["scientific_name"]} for k in choising]
    print(flower)
    
    quizz.flowers = flower
    quizz.the_question = the_question
    print(quizz)
    return flower, the_question

app = Flask(__name__,static_folder='static')

@app.route('/')
def home():
    return render_template("index.html")

# @app.route('/')
# def setcookie():
#     resp = make_response(f"test")
#     resp.set_cookie("SameSite","Strict")
#     resp.set_cookie('somecookiename', 'I am cookie')
#     resp.set_cookie('userID', secrets.token_hex())
#     return resp

# @app.route('/getcookie')
# def getcookie():
#     name = request.cookies.get('userID')
#     return f"The Site : {name}"

@app.route('/game')
def game():
    flower, question = choice_4flower(api_get_data_flower())
    return render_template('game.html',flower=flower, quiz_responce=question['sci_name'], img_url=question['img_url'], responce_id=question['id'])

@app.route('/game',methods=['POST'])
def get_data_user_form():
    data = request.form.to_dict()
    print(data)
    print("-"*10)
    if data["btnflower"] == data["btnresponse"] :
        return render_template("congrat.html")
    else :
        return render_template("try_again.html")

@app.route('/rules')
def rules():
    return render_template('rules.html')