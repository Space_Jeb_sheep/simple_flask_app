DROP TABLE IF EXISTS flower;

CREATE TABLE flower (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name varchar(255),
    name_sci varchar(500),
    image_url varchar(2500),
    image_author varchar(255),
    wiki_url varchar(255)
);

