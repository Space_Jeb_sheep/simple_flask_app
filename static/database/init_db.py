import sqlite3


import sys
print(sys.path)

connection = sqlite3.connect(sys.path[0]+'/database.db')

with open(sys.path[0]+'/schema.sql') as f:
    connection.executescript(f.read())
cur = connection.cursor()
cur.execute("INSERT INTO flower (name, name_sci) VALUES (?, ?)",
            ('Camellia', 'Camellia sasanqua')
            )
cur.execute("INSERT INTO flower (name, name_sci) VALUES (?, ?)",
            ('Petite Pervenche', 'Vinca minor')
            )
cur.execute("INSERT INTO flower (name, name_sci) VALUES (?, ?)",
            ('Pensée des champs', 'Viola arvensis')
            )
cur.execute("INSERT INTO flower (name, name_sci) VALUES (?, ?)",
            ('Scabiosa', 'Scabiosa atropurpurea')
            )
cur.execute("INSERT INTO flower (name, name_sci) VALUES (?, ?)",
            ('Populage des marais', 'Caltha palustris')
            )
connection.commit()
connection.close()